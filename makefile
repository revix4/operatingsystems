Project1: pa1_part1 pa1_part2 pa1_part3

pa1_part1: pa1_part1.o
	gcc -o pa1_part1 pa1_part1.o -lrt

pa1_part1.o: pa1_part1.c
	gcc -c pa1_part1.c

pa1_part2: pa1_part2.o
	gcc -o pa1_part2 pa1_part2.o

pa1_part2.o: pa1_part2.c
	gcc -c pa1_part2.c

pa1_part3.o: pa1_part3.c
	gcc -c pa1_part3.c
