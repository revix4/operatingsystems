#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <unistd.h>
#include <semaphore.h>

static int globalVar;
sem_t mutex;

enum ProcessState
{
	Terminated = -1,
	Waiting,
	Ready
};

struct Process
{
	int PID;
	enum ProcessState state;
};

int main(void)
{
	int numberOfChildren = 5;
	struct Process children[5];
	int i, actionsPerTurn = 4, turnCount = 0;

	sem_init(&mutex, 1, 1);

	globalVar = 0;

	for(i = 0; i < numberOfChildren; i++)
	{
		children[i].PID = i+1;
		children[i].state = Ready;
	}

	while(numberOfChildren > 0)
	{
		for(i = 0; i < actionsPerTurn; i++)
		{
			if(globalVar > 99)
			{
				children[turnCount%5].state = Terminated;
				numberOfChildren--;
				turnCount++;
				break;
			}
			if(children[turnCount%5].state == Ready)
			{
				sem_wait(&mutex);

				globalVar++;

				printf("Global var %i Child PID %i\n",
					globalVar, 
					children[turnCount%5].PID);

				sem_post(&mutex);
			}
		}
		turnCount++;
	}

	sem_destroy(&mutex);
	
	return 0;
}
