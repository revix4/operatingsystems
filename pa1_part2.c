#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

static int globalVar;

enum ProcessState
{
	Terminated = -1,
	Waiting,
	Ready
};

struct Process
{
	int PID;
	int var;
	enum ProcessState state;
};

int main(void)
{
	int numberOfChildren = 5;
	struct Process children[5];
	int i, actionsPerTurn = 4, turnCount = 0;
	int mypipe[2];

	globalVar = 0;

	if(pipe(mypipe))
	{
		fprintf(stderr, "Pipe fail\n");
		return EXIT_FAILURE;
	}

	write(mypipe[1], &globalVar, sizeof(int));

	for(i = 0; i < numberOfChildren; i++)
	{
		children[i].PID = i+1;
		children[i].var = 0;
		children[i].state = Ready;
	}

	while(numberOfChildren > 0)
	{
		for(i = 0; i < actionsPerTurn; i++)
		{	
			read(mypipe[0], &children[turnCount%5].var, sizeof(int));
			if(children[turnCount%5].var > 99)
			{
				write(mypipe[1], &children[turnCount%5].var,
					 sizeof(int));
				children[turnCount%5].state = Terminated;
				numberOfChildren--;
				turnCount++;
				break;
			}
			if(children[turnCount%5].state == Ready)
			{
				children[turnCount%5].var = 
					children[turnCount%5].var + 1;

				printf("Global var %i Child PID %i\n",
					children[turnCount%5].var, 
					children[turnCount%5].PID);

				write(mypipe[1], &children[turnCount%5].var,
					 sizeof(int));

			}
		}
		turnCount++;
	}
	
	return 0;
}
