#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>

static int *globalVar;

enum ProcessState
{
	Terminated = -1,
	Waiting,
	Ready
};

struct Process
{
	int PID;
	int var;
	enum ProcessState state;
};

int main(void)
{
	int numberOfChildren = 5;
	struct Process children[5];
	int i, actionsPerTurn = 4, turnCount = 0;

	globalVar = mmap(NULL, sizeof *globalVar, PROT_READ | PROT_WRITE,
			MAP_SHARED | MAP_ANONYMOUS, -1, 0);

	*globalVar = 0;

	for(i = 0; i < numberOfChildren; i++)
	{
		children[i].PID = i+1;
		children[i].var = 0;
		children[i].state = Ready;
	}

	while(numberOfChildren > 0)
	{
		for(i = 0; i < actionsPerTurn; i++)
		{
			if(*globalVar > 99)
			{
				children[turnCount%5].state = Terminated;
				numberOfChildren--;
				turnCount++;
				break;
			}
			if(children[turnCount%5].state == Ready)
			{
				children[turnCount%5].var = *globalVar;
				children[turnCount%5].var++;
				*globalVar = children[turnCount%5].var;

				printf("Global var %i Child PID %i\n",
					children[turnCount%5].var, 
					children[turnCount%5].PID);
			}
		}
		turnCount++;
	}

	munmap(globalVar, sizeof *globalVar);
	
	return 0;
}
